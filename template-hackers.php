<?php
	/* Template Name: Hackers User Page Template */
	get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>

			<h1><?php _e( 'Hackers', 'projectnight' ); ?></h1>

			<?php
				// Check for projects page content
				$hackers_page = get_posts( array( 'name' => 'hackers', 'post_type' => 'page' ) );
				if( $hackers_page[0] ) {
					echo apply_filters( 'the_content', $hackers_page[0]->post_content );
				}

				// Get a list of all users
				$hackers = get_users( array( 'order_by' => 'registered', 'order' => 'DESC' ) );
				// Check for results
				if (!empty($hackers)) {
					
					echo '<ul class="hackers-list">';
					
					// loop trough each hacker
					foreach ($hackers as $hacker) {
						// get all the user's data
						$hacker_info = get_userdata($hacker->ID);

						echo '<li class="hacker-profile" id="' . $hacker_info->user_nicename . '">'
							.'	' . get_avatar($hacker->ID,320)
							.'	<div class="hacker-info">'
							.'		<span class="hacker-name">' . $hacker_info->display_name . '</span>'
							.'		<ul class="hacker-links">';
							
							$hacker_links = array(
								'twitter' => get_field('hacker_twitter','user_'.$hacker_info->ID),
								'github' => get_field('hacker_github','user_'.$hacker_info->ID),
								'dribble' => get_field('hacker_dribbble','user_'.$hacker_info->ID),
								'flickr' => get_field('hacker_flickr','user_'.$hacker_info->ID),
								'googleplus' => get_field('hacker_googleplus','user_'.$hacker_info->ID),
								'facebook' => get_field('hacker_facebook','user_'.$hacker_info->ID),
								'instagram' => get_field('hacker_instagram','user_'.$hacker_info->ID),
								'lastfm' => get_field('hacker_lastfm','user_'.$hacker_info->ID)
							);

							$link_count = 0;
							foreach ($hacker_links as $site => $link) {
								if ( $link != "" && $link_count < 6) {
									$link_count++;
									if ( $site == 'twitter' ) { $link = 'https://twitter.com/' . $link; }
									if ( $site == 'github' ) { $link = 'https://github.com/' . $link; }
									if ( $site == 'dribble' ) { $link = 'https://dribbble.com/' . $link; }
									if ( $site == 'flickr' ) { $link = 'https://www.flickr.com/photos/' . $link; }
									if ( $site == 'googleplus' ) { $link = 'https://plus.google.com/' . $link; }
									if ( $site == 'facebook' ) { $link = 'https://facebook.com/' . $link; }
									if ( $site == 'instagram' ) { $link = 'https://instagram.com/' . $link; }
									if ( $site == 'lastfm' ) { $link = 'https://www.last.fm/user/' . $link; }
									echo '			<li class="' . $site . '"><a href="' . $link . '" title="' . $site . '" target="_blank"><span>' . $site . '</span></a></li>';
								}
							}

						echo '		</ul>'
							.'	</div>'
							.'</li>';
					}
					
					echo '</ul>';

				} else {

					echo 'No hackers found';

				}
			?>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
