<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<!-- article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class('one two'); ?>>

		<!-- post title -->
		<h2 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>

		<!-- post thumbnail -->
		<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="responsive post-thumbnail">
				<?php the_post_thumbnail('featured-medium'); ?>
			</a>
		<?php endif; ?><!-- /post thumbnail -->

		<!-- post meta -->
		<div class="post-meta">
			<?php 
				// Count number of contributors
				$hackers = get_field('project_authors');
				if ( $hackers ) {
					$hackers = count($hackers);
					if( $hackers == 1) {
						$hackers = $hackers . " contributor";
					} else {
						$hackers = $hackers . " contributors";
					}
				} else {
					$hackers = "No contributors listed";
				}
			?>
			<span class="authors"><?php echo $hackers; ?></span>
			<span class="date">Added: <?php the_time('M j, Y'); ?></span>
		</div><!-- /post meta -->

	</article>
	<!-- /article -->

<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'projectnight' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>
