<?php

	// To start grouping things by month, here's an empty month group placeholder.
	$month_group = NULL;

	if (have_posts()): while (have_posts()) : the_post();

		// Take the post's current month from the start date 
		$current_month = date('F Y', get_field('event_start_time'));
		// Check to see if the post's current month is the same as the month group
		if ( $current_month != $month_group ) {
			// If != then start a new month group and display a header.
			$month_group = $current_month;
			echo '<h2 class="month-group">' . $current_month . '</h2>';
		}
?>

	<!-- article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<!-- post title -->
		<h2 class="event-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>

		<!-- post thumbnail -->
		<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="post-thumbnail">
				<?php the_post_thumbnail('featured-large'); ?>
			</a>
		<?php endif; ?>
		<!-- /post thumbnail -->

		<div class="event-info">
			<!-- event date -->
			<div class="event-date">
				<div class="calendar">
			<?php

				// Get start and (if exists) end dates:
				$event_start = get_field('event_start_time');
				$event_end = get_field('event_end_time');

				// Get the weekday from the start date: ?>
					<span class="weekday"><?php echo date('l',$event_start); ?></span>

			<?php
				// Get the month from the start date: ?>
					<span class="month"><?php echo date('F',$event_start); ?></span>

			<?php
				// Get the event day or days:		
				$start_day = date('j',$event_start);
				$day = $start_day;
				// IF - there is an end date set, AND it's not on the same day AND it's not in the next month...
				if ( $event_end && ( $start_day != date('j', $event_end) ) && ( ( date('F',$event_start) ) == date('F',$event_end) ) ) {
					// THEN - Set the end day:
					$end_day = date('j', $event_end);
					$day = $start_day . ' - ' . $end_day;
				} ?>
					<span class="day"><?php echo $day; ?></span>
				</div>

			<?php
				// IF - This isn't a multi day event, include the time.
				if ( !isset($end_day) ) {
					// IF - There is an end date set (and we know it's not on a different day)
					if ( $event_end ) {
						$time = date('g:i',$event_start) . ' - ' . date('g:i A',$event_end);
					} else {
						$time = date('h:i A',$event_start);
					}
				}
			?>
				<span class="time"><?php echo $time; ?></span>
			<?php
				// Clean up existing date values.
				unset($start_day,$end_day,$time); ?>
			</div>
			<div class="event-details">
				<span class="author"><?php _e( 'Submitted by', 'projectnight' ); ?> <?php the_author_posts_link(); ?></span>
				<div class="event-description">
					<?php the_content(); ?>
				</div>
			</div>
		
	</article>
	<!-- /article -->

<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, no events to display.', 'projectnight' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>