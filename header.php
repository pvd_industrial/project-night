<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' : '; } ?><?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,700,400italic' rel='stylesheet' type='text/css'>
		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>

		<div class="wrapper clearfix">

			<!-- header -->
			<header class="header clearfix" role="banner">

				<!-- branding -->
				<div class="branding">
					<a href="<?php echo home_url(); ?>" class="home-link">Boston Hack Night</a>
				</div><!-- /branding -->

				<!-- feature-image -->
				<div class="feature-image">
					<img src="<?php echo get_template_directory_uri() ?>/img/boston-01.png" />
					<?php
						if ( is_active_sidebar('home-feature') && is_front_page() ) {
							echo '<div class="home-feature">';
							dynamic_sidebar('home-feature');
							echo '</div>';
						}
					?>
					<span class="photo-credit">Photo Credit: <a href="https://www.flickr.com/photos/11108455@N00/322151317/">paul+photos=moody</a> [<a href="https://creativecommons.org/licenses/by-nc/2.0/">CC BY-NC</a>]</span>
				</div><!-- /feature-image -->

				<!-- nav -->
				<nav class="nav" role="navigation">
					<?php projectnight_nav(); ?>
				</nav><!-- /nav -->

			</header>
			<!-- /header -->
