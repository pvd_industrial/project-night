<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>

			<h1><?php _e( 'Projects', 'projectnight' ); ?></h1>

			<?php
				// Check for projects page content
				$projects_page = get_posts( array( 'name' => 'projects', 'post_type' => 'page' ) );
				if( $projects_page[0] ) {
					echo apply_filters( 'the_content', $projects_page[0]->post_content );
				}
			?>

			<?php get_template_part('loop-projects'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
